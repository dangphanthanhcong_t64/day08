<?php
session_start();
$genders = array(
    0 => "Nam",
    1 => "Nữ",
);
$faculties = array(
    "None" => "",
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu",
);

// define variables to empty values
$name = $gender = $faculty = $birth = $address = $image = "";
$_SESSION["name"] = $_SESSION["gender"] = $_SESSION["faculty"] = $_SESSION["birth"] = $_SESSION["address"] = $_SESSION["image"] = "";
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Register</title>
</head>

<body>
    <main>
        <form action="register.php" method="post" enctype="multipart/form-data">
            <div class="container">
                <?php
                // fields validation
                $valid = true;
                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    // name validation
                    if (empty($_POST["name"])) {
                        echo "<font color='red'>Hãy nhập tên.</font> </br>";
                        $valid = false;
                    } else {
                        $_SESSION["name"] = $_POST["name"];
                    }

                    // gender validation
                    if (!isset($_POST["gender"])) {
                        echo "<font color='red'>Hãy chọn giới tính.</font> </br>";
                        $valid = false;
                    } else {
                        $_SESSION["gender"] = $genders[$_POST["gender"]];
                    }

                    // faculty validation
                    if ($_POST["faculty"] == "None") {
                        echo "<font color='red'>Hãy chọn phân khoa.</font> </br>";
                        $valid = false;
                    } else {
                        $_SESSION["faculty"] = $faculties[$_POST["faculty"]];
                    }

                    // birth validation
                    if (empty($_POST["birth"])) {
                        echo "<font color='red'>Hãy nhập ngày sinh.</font> </br>";
                        $valid = false;
                    } else {
                        $_SESSION["birth"] = $_POST["birth"];
                        $birth_arr  = explode('/', $_POST["birth"]);
                        if (count($birth_arr) != 3 || !checkdate($birth_arr[1], $birth_arr[0], $birth_arr[2])) {
                            echo "<font color='red'>Hãy nhập ngày sinh đúng định dạng.</font> </br>";
                            $valid = false;
                        }
                    }

                    // address
                    $_SESSION["address"] = $_POST["address"];

                    // image validation
                    if (file_exists($_FILES["image"]["tmp_name"])) {
                        $check = @getimagesize($_FILES["image"]["tmp_name"]);
                        if ($check == false) {
                            echo "<font color='red'>Hãy chọn tệp tin là ảnh.</font> </br>";
                            $valid = false;
                        }
                    }

                    if ($valid) {
                        $target_dir = "day05/upload/";

                        // checking whether file exists or not
                        if (!file_exists($target_dir)) {
                            // create a new file or directory
                            mkdir($target_dir, 0777, true);
                        }

                        $target_file = $target_dir . basename($_FILES["image"]["name"]);
                        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

                        date_default_timezone_set('Asia/Ho_Chi_Minh');
                        $date = date("YmdHis");

                        $extension = end(explode(".", $target_file));
                        $new_target_file =  $target_dir . basename($target_file, ".$extension") . "_$date." . $extension;
                        rename($target_file, $new_target_file);
                        $_SESSION["image"] = $new_target_file;

                        header("location: confirmation.php");
                        exit();
                    }
                }
                ?>
                <div>
                    <label for="name">Họ và tên<span class="required">*</span></label>
                    <input type="text" class="input" name="name" value='<?php echo ($_SESSION["name"]); ?>'>
                </div>

                <div>
                    <label for="gender">Giới tính<span class="required">*</span></label>
                    <?php
                    for ($i = 0; $i < count($genders); $i++) {
                        if ($genders[$i] == $_SESSION["gender"]) {
                            $check = "checked";
                        } else {
                            $check = "";
                        }
                        echo "<input type='radio' id=$i name='gender' value=$i $check>$genders[$i] ";
                    }
                    ?>
                </div>

                <div>
                    <label for="faculty">Phân khoa<span class="required">*</span></label>
                    <select name='faculty'>
                        <?php
                        foreach ($faculties as $key => $value) {
                            if ($faculties[$key] == $_SESSION["faculty"]) {
                                $selected = "selected";
                            } else {
                                $selected = "";
                            }
                            echo "<option value=$key $selected>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div>
                    <label for="birth">Ngày sinh<span class="required">*</span></label>
                    <input type="text" class="input" name="birth" placeholder="dd/mm/yyyy" value='<?php echo ($_SESSION["birth"]); ?>'>
                </div>

                <div>
                    <label for="address">Địa chỉ</label>
                    <input type="text" class="input" name="address" value='<?php echo ($_SESSION["address"]); ?>'>
                </div>

                <div>
                    <label for="image">Hình ảnh</label>
                    <input type="file" name="image">
                </div>

                <button type="submit" class="button" name="register">Đăng ký</button>
        </form>
    </main>
</body>

</html>